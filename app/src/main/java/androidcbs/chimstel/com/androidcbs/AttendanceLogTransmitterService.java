package androidcbs.chimstel.com.androidcbs;

import java.util.concurrent.ExecutionException;

/**
 * The background service for transmitting sms messages to the backend
 */
public class AttendanceLogTransmitterService {

    public static final String APPLICATION_BASE_URL = "app.nilecom.co.ug";
    public static final String ATTENDANCE_LOG_DELIVERY_URL = "http://app.nilecom.co.ug/api/attendancelogs";


    /**
     *
     * @param log - the SMS Message to transmit to the server
     * @return - an integer that represents the status of the HttpResponse 200 for success, 409 for conflict and default for anything else
     */
    public Integer SendLog (AttendanceLog log) {
        SendAttendanceLogMessageAsync sendLogAsync = new SendAttendanceLogMessageAsync(log);
        try{
            return sendLogAsync.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
