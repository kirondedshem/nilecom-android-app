package androidcbs.chimstel.com.androidcbs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * The repository - an abstraction of the underlying data-source that holds our SMS messages
 */
public class AttendanceLogRepository {

    private DatabaseHelper databaseHelper;

    /**
     *
     * @param databaseHelper - a wrapper around the database (more like an access helper)
     */
    public AttendanceLogRepository(DatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    /**
     *
     * @param attendance_log - an Attendance Log to be inserted into the local repository
     * @return - an Integer that indicates the status of the application
     */
    public int insert(AttendanceLog attendance_log) {
        int returnVal = Integer.MIN_VALUE;
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(AttendanceLog.ATTENDANCE_LOG_DEVICE_IMEI, attendance_log.getAttendanceLogDeviceIMEI());
            values.put(AttendanceLog.ATTENDANCE_LOG_ID, attendance_log.getAttendanceLogId());
            values.put(AttendanceLog.ATTENDANCE_LOG_LATITUDE, attendance_log.getAttendanceLogLatitude());
            values.put(AttendanceLog.ATTENDANCE_LOG_LOG_TIME, attendance_log.getAttendanceLogLogTime());
            values.put(AttendanceLog.ATTENDANCE_LOG_LONGITUDE, attendance_log.getAttendanceLogLongitude());
            values.put(AttendanceLog.ATTENDANCE_LOG_SIM_CARD_SERIAL, attendance_log.getAttendanceLogSimCardSerial());
            values.put(AttendanceLog.ATTENDANCE_LOG_LOG_TYPE, attendance_log.getAttendanceLogLogType());
            long AttendanceLogId = db.insert(AttendanceLog.ATTENDANCE_LOG_TABLE, null, values);
            db.close();
            returnVal = (int) AttendanceLogId;
        }catch(Exception e){
            e.printStackTrace();
        }
        return returnVal;
    }

    /**
     *
     * @param AttendanceLogId - the unique identifier of the SMS to be deleted from the repository
     * @throws Exception - an Exception in case anything goes wrong
     */
    public void delete(String AttendanceLogId) throws Exception {
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            String sql = "DELETE FROM " + AttendanceLog.ATTENDANCE_LOG_TABLE + " WHERE " + AttendanceLog.ATTENDANCE_LOG_ID  + "= ?";
            db.execSQL(sql, new String[]{AttendanceLogId});
            db.close();
            Log.i("Attendance Repository", "Attendance Log Deleted");
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     *
     * @return - a list of Attendance Logs that are saved in the repository at any given time
     */
    public List<AttendanceLog> getAllAttendanceLogs(){
        List<AttendanceLog> AttendanceLogList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + AttendanceLog.ATTENDANCE_LOG_TABLE;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                AttendanceLog aLog = new AttendanceLog();
                aLog.setAttendanceLogId(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_ID)));
                aLog.setAttendanceLogLogType(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_LOG_TYPE)));
                aLog.setAttendanceLogDeviceIMEI(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_DEVICE_IMEI)));
                aLog.setAttendanceLogSimCardSerial(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_SIM_CARD_SERIAL)));
                aLog.setAttendanceLogLogTime(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_LOG_TIME)));
                aLog.setAttendanceLogLatitude(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_LATITUDE)));
                aLog.setAttendanceLogLongitude(cursor.getString(cursor.getColumnIndex(AttendanceLog.ATTENDANCE_LOG_LONGITUDE)));


                AttendanceLogList.add(aLog);
            }while(cursor.moveToNext());
            cursor.close();
        }
        db.close();
        Log.d("attendanceLogList",AttendanceLogList.toArray().toString());
        return AttendanceLogList;
    }
}
