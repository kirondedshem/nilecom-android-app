package androidcbs.chimstel.com.androidcbs;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class PeriodicAttendanceLogTransmitterService extends Service {

    private Thread CurrentThread = null;

    Handler mOnlineApiActivityCompleteHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            try{

                attendanceLogList = LogRepository.getAllAttendanceLogs();
                //reschedule
                //schedule to run after 1 minute
                handler.postDelayed(OnElapseExecute(), 20 * 1000);



            } catch (Exception e) {
                Log.e("Service Qeueing", e.getMessage());
                e.printStackTrace();
            }

        }
    };


    public Runnable OnlineActivityComplete() {

        return new Runnable() {
            public void run() {

                //Message message = mHandler.obtainMessage(command, parameter);
                Message message = mOnlineApiActivityCompleteHandler.obtainMessage();
                message.sendToTarget();


            }

        };

    }


    private List<AttendanceLog> attendanceLogList;
    private AttendanceLogRepository LogRepository;
    private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    //set it global so u can call it manually
    Handler handler = new Handler();

    @Override
    public IBinder onBind(Intent intent) { throw new UnsupportedOperationException("Not yet implemented"); }

    @Override
    public void onCreate() {
        super.onCreate();
        if(LogRepository == null) LogRepository = new AttendanceLogRepository(new DatabaseHelper(getBaseContext()));
        attendanceLogList = LogRepository.getAllAttendanceLogs();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public Runnable OnElapseExecute()
    {
        return new Runnable() {
            @Override
            public void run() {
                Log.i("Period SMS Transmitter", "Sms transmitter onHandle Intent - runnable section");
                SyncData();
            }
        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        //schedule to run after 1 minute
        handler.postDelayed(OnElapseExecute(), 20 * 1000);

        return START_STICKY;
    }

    /**
     * Synchronizes the data with the backend in the case that we have any un-logged transactions' data
     */
    private void SyncData(){



        CurrentThread = new Thread()
        {
            public void run()
            {

                try
                {


                    // read an sms from the list and transmit it
                    for(AttendanceLog log : attendanceLogList){
                        Log.i("Unsent Attendance Log", log.toString());

                        Integer result = new AttendanceLogTransmitterService().SendLog(log);
                        switch(result){
                            case 200:case 409: {

                                //record last send time
                                try
                                {
                                    String theTime = dateFormat.format(NetworkTime.getNetworkDateTime(getBaseContext()));
                                    SharedPreferences sharedPref = getBaseContext().getSharedPreferences(getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString(getBaseContext().getString(R.string.last_server_communication_time), theTime);
                                    editor.commit();
                                }
                                catch(Exception e){
                                    e.printStackTrace();
                                }

                                LogRepository.delete(log.getAttendanceLogId());
                                Log.i("SMS Sent", log.toString());
                                break;
                            }
                            default: break;
                        }

                    }






                }
                catch (Exception e)
                {
                    Log.e("Thread Exception",e.getMessage());
                }
                finally {
                    try {

                        //run the handler
                        OnlineActivityComplete().run();
                    } catch (Exception exc)
                    {
                        exc.printStackTrace();
                    }
                }


            }
        };
        CurrentThread.start();




    }


}
