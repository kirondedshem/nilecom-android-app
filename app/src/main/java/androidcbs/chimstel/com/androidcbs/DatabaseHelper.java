package androidcbs.chimstel.com.androidcbs;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "sms_db";

    //indicate an upgrade
    private static final int DATABASE_VERSION = 2;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TABLE_SMS_MESSAGE = "CREATE TABLE " + SMSMessage.SMS_MESSAGE_TABLE + "("
                + SMSMessage.SMS_MESSAGE_ID + " VARCHAR(40) PRIMARY KEY, "
                + SMSMessage.SMS_MESSAGE_CONTENT + " VARCHAR (200) NOT NULL, "
                + SMSMessage.SMS_MESSAGE_DEVICE_IMEI + " VARCHAR (20) NOT NULL, "
                + SMSMessage.SMS_MESSAGE_SIM_CARD_SERIAL + " VARCHAR (30) NOT NULL, "
                + SMSMessage.SMS_MESSAGE_LATITUDE + " VARCHAR (30) NULL, "
                + SMSMessage.SMS_MESSAGE_LONGITUDE + " VARCHAR (30) NULL, "
                + SMSMessage.SMS_MESSAGE_LOG_TIME + " VARCHAR (40) NOT NULL,"
                + SMSMessage.SMS_MESSAGE_REFERENCE_TIME + " VARCHAR (40) NOT NULL)";
        db.execSQL(CREATE_TABLE_SMS_MESSAGE);

        String CREATE_TABLE_LOG_MESSAGE = "CREATE TABLE " + LogObject.LOGGER_TABLE + "("
                + LogObject.LOGGER_MESSAGE_ID + " VARCHAR(200) PRIMARY KEY, "
                + LogObject.LOGGER_MESSAGE_NETWORK_TIME + " VARCHAR (200) NOT NULL, "
                + LogObject.LOGGER_MESSAGE_SIM_SERIAL + " VARCHAR (20) NOT NULL, "
                + LogObject.LOGGER_MESSAGE_LATITUDE + " VARCHAR (30) NOT NULL, "
                + LogObject.LOGGER_MESSAGE_LONGITUDE + " VARCHAR (30) NOT NULL, "
                + LogObject.LOGGER_MESSAGE_CONTENT + " VARCHAR (30) NOT NULL, "
                + LogObject.LOGGER_MESSAGE_DEVICE_IMEI + " VARCHAR (40) NOT NULL)";
        db.execSQL(CREATE_TABLE_LOG_MESSAGE);

        String CREATE_TABLE_ATTENDACE_LOG = "CREATE TABLE " + AttendanceLog.ATTENDANCE_LOG_TABLE + "("
                + AttendanceLog.ATTENDANCE_LOG_ID + " VARCHAR(40) PRIMARY KEY, "
                + AttendanceLog.ATTENDANCE_LOG_LOG_TYPE + " VARCHAR (200) NOT NULL, "
                + AttendanceLog.ATTENDANCE_LOG_DEVICE_IMEI + " VARCHAR (20) NOT NULL, "
                + AttendanceLog.ATTENDANCE_LOG_SIM_CARD_SERIAL + " VARCHAR (30) NOT NULL, "
                + AttendanceLog.ATTENDANCE_LOG_LATITUDE + " VARCHAR (30) NULL, "
                + AttendanceLog.ATTENDANCE_LOG_LONGITUDE + " VARCHAR (30) NULL, "
                + AttendanceLog.ATTENDANCE_LOG_LOG_TIME + " VARCHAR (40) NOT NULL)";
        db.execSQL(CREATE_TABLE_ATTENDACE_LOG);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

        /*
        // Drop older table if it existed, all data will be gone
        String DELETE_TABLE_SMS_MESSAGE = "DROP TABLE IF EXISTS " + SMSMessage.SMS_MESSAGE_TABLE;
        db.execSQL(DELETE_TABLE_SMS_MESSAGE);
        String DELETE_TABLE_LOG_MESSAGE = "DROP TABLE IF EXISTS " + LogObject.LOGGER_TABLE ;
        db.execSQL(DELETE_TABLE_LOG_MESSAGE);
        String DELETE_TABLE_ATTENDANCE_LOG = "DROP TABLE IF EXISTS " + AttendanceLog.ATTENDANCE_LOG_TABLE ;
        db.execSQL(DELETE_TABLE_ATTENDANCE_LOG);

        //recreate new database strcuture
        onCreate(db);

        */

        if((oldVersion == 1) && (newVersion == 2)) {
            //just so we dont loose data
            String CREATE_TABLE_ATTENDACE_LOG = "CREATE TABLE " + AttendanceLog.ATTENDANCE_LOG_TABLE + "("
                    + AttendanceLog.ATTENDANCE_LOG_ID + " VARCHAR(40) PRIMARY KEY, "
                    + AttendanceLog.ATTENDANCE_LOG_LOG_TYPE + " VARCHAR (200) NOT NULL, "
                    + AttendanceLog.ATTENDANCE_LOG_DEVICE_IMEI + " VARCHAR (20) NOT NULL, "
                    + AttendanceLog.ATTENDANCE_LOG_SIM_CARD_SERIAL + " VARCHAR (30) NOT NULL, "
                    + AttendanceLog.ATTENDANCE_LOG_LATITUDE + " VARCHAR (30) NULL, "
                    + AttendanceLog.ATTENDANCE_LOG_LONGITUDE + " VARCHAR (30) NULL, "
                    + AttendanceLog.ATTENDANCE_LOG_LOG_TIME + " VARCHAR (40) NOT NULL)";
            db.execSQL(CREATE_TABLE_ATTENDACE_LOG);

        }


    }
}
