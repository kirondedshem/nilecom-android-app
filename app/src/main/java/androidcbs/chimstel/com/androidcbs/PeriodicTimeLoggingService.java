package androidcbs.chimstel.com.androidcbs;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.List;

public class PeriodicTimeLoggingService extends Service {


    private Thread CurrentThread = null;

    Handler mOnlineApiActivityCompleteHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            try{

                logObjects = loggerRepository.getAllLoggerMessages();
                //reschedule
                //schedule to run after 1 minute
                handler.postDelayed(OnElapseExecute(), 20 * 1000);



            } catch (Exception e) {
                Log.e("Service Qeueing", e.getMessage());
                e.printStackTrace();
            }

        }
    };


    public Runnable OnlineActivityComplete() {

        return new Runnable() {
            public void run() {

                //Message message = mHandler.obtainMessage(command, parameter);
                Message message = mOnlineApiActivityCompleteHandler.obtainMessage();
                message.sendToTarget();


            }

        };

    }

    //set it global so u can call it manually
    android.os.Handler handler = new android.os.Handler();


    private List<LogObject> logObjects;
    private LoggerRepository loggerRepository;
    public PeriodicTimeLoggingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Toast.makeText(getBaseContext() ,"created service",Toast.LENGTH_LONG);
        if(loggerRepository == null) loggerRepository = new LoggerRepository(new DatabaseHelper(getBaseContext()));
        logObjects = loggerRepository.getAllLoggerMessages();
        //Toast.makeText(getBaseContext() ,"successfully gor messages",Toast.LENGTH_LONG);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        //schedule to run after 1 minute
        handler.postDelayed(OnElapseExecute(), 20 * 1000);

        return START_STICKY;
    }

    public Runnable OnElapseExecute()
    {
        return new Runnable() {
            @Override
            public void run() {
                Log.i("Period SMS Transmitter", "Sms transmitter onHandle Intent - runnable section");
                SyncData();
            }
        };
    }

    /**
     * Synchronizes the data with the backend in the case that we have any un-logged transactions' data
     */
    private void SyncData(){





        CurrentThread = new Thread()
        {
            public void run()
            {

                try
                {


                    for(LogObject logSms :logObjects){
                        //Log.i("Unsent Log Object SMS", logSms.toString());
                        Integer result = new LogTransmitterService().sendLogObject(logSms);
                        switch(result){
                            case 200:case 409:
                                loggerRepository.delete(logSms.getLoggerMsgId());
                                break;
                            default: break;
                        }
                    }




                }
                catch (Exception e)
                {
                    //Log.e("Thread Exception",e.getMessage());
                }
                finally {
                    try {

                        //run the handler
                        OnlineActivityComplete().run();
                    } catch (Exception exc)
                    {
                        exc.printStackTrace();
                    }
                }


            }
        };
        CurrentThread.start();


    }



}
