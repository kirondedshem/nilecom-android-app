package androidcbs.chimstel.com.androidcbs;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class Start extends AppCompatActivity {

    private Thread CurrentThread = null;
    Button btn_add_attendance_log;
    Button btn_read_inbox;
    List<Sms> inbox_SmsList = new ArrayList<>();

    Handler mOnlineApiActivityCompleteHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            try{

                btn_read_inbox.setEnabled(true);
                btn_add_attendance_log.setEnabled(true);

            } catch (Exception e) {
                e.printStackTrace();
            }

            try{

                Toast.makeText(Start.this,"Read Inbox Completed, found:"+inbox_SmsList.size(),Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };


    public Runnable ReadInboxComplete() {

        return new Runnable() {
            public void run() {

                //Message message = mHandler.obtainMessage(command, parameter);
                Message message = mOnlineApiActivityCompleteHandler.obtainMessage();
                message.sendToTarget();


            }

        };

    }


    private class Sms{
        private String _id;
        private String _address;
        private String _msg;
        private String _readState; //"0" for have not read sms and "1" for have read sms
        private String _time;
        private String _time_str;
        private String _folderName;

        public String getId(){
            return _id;
        }
        public String getAddress(){
            return _address;
        }
        public String getMsg(){
            return _msg;
        }
        public String getReadState(){
            return _readState;
        }
        public String getTime(){
            return _time;
        }
        public String getTimeStr(){
            return _time_str;
        }
        public String getFolderName(){
            return _folderName;
        }


        public void setId(String id){
            _id = id;
        }
        public void setAddress(String address){
            _address = address;
        }
        public void setMsg(String msg){
            _msg = msg;
        }
        public void setReadState(String readState){
            _readState = readState;
        }
        public void setTime(String time){
            _time = time;
        }
        public void setTimeStr(String time_str){
            _time_str = time_str;
        }
        public void setFolderName(String folderName){
            _folderName = folderName;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        try
        {
            // Periodic SMS Transmitter Service
            Intent newIntent = new Intent(this, PeriodicAttendanceLogTransmitterService.class);
            this.startService(newIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Periodic SMS Transmitter Service
            Intent smsTransmitterIntent = new Intent(this, PeriodicSMSTransmitterService.class);
            this.startService(smsTransmitterIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Location Service
            Intent locationIntent = new Intent(this, NetworkLocationService.class);
            this.startService(locationIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Periodic Logs Transmitter
            Intent logServiceIntent = new Intent(this, PeriodicTimeLoggingService.class);
            this.startService(logServiceIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Time and Attendance Logs Service
            Intent timerIntent = new Intent(this, TimerService.class);
            this.startService(timerIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            Button btn_refresh = (Button)this.findViewById(R.id.btn_refresh);
            btn_refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateInterface();

                    Toast.makeText(Start.this,"Content refreshed",Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Button Event",exc.getMessage());
        }


        try
        {
            btn_read_inbox = (Button)this.findViewById(R.id.btn_read_sms_inbox);
            btn_read_inbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    btn_read_inbox.setEnabled(false);
                    btn_add_attendance_log.setEnabled(false);
                    getAllSms(Start.this);

                    UpdateInterface();

                }
            });
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Button Event",exc.getMessage());
        }


        try
        {
            btn_add_attendance_log = (Button)this.findViewById(R.id.btn_add_attendance_log);
            btn_add_attendance_log.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SaveAttendanceLog("CheckIn");

                    Toast.makeText(getApplicationContext(), "Checked In Successfully", Toast.LENGTH_SHORT).show();

                    //disable the button
                    btn_read_inbox.setEnabled(false);
                    btn_add_attendance_log.setEnabled(false);

                    getAllSms(Start.this);




                    UpdateInterface();

                }
            });
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Button Event",exc.getMessage());
        }


        try
        {
            UpdateInterface();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Update Interface",exc.getMessage());
        }









       // Toast.makeText(getApplicationContext(), "Services Started", Toast.LENGTH_SHORT).show();
    }



    private void SaveAttendanceLog(String Log_Type)
    {
        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        Location deviceLocation = NetworkLocationService.previousBestLocation;
        if(deviceLocation == null){
            Log.i("SMS Receiver", "Device Location is null");
        }else{
            Log.i("SMS Receiver", "Device Location has a value");
        }

        AttendanceLog attendance_log = new AttendanceLog();
        attendance_log.setAttendanceLogId(UUID.randomUUID().toString());
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String theTime = dateFormat.format(NetworkTime.getNetworkDateTime(getBaseContext()));
        attendance_log.setAttendanceLogLogTime(theTime);
        attendance_log.setAttendanceLogLogType(Log_Type);

        attendance_log.setAttendanceLogDeviceIMEI(telephonyManager.getDeviceId());
        attendance_log.setAttendanceLogLatitude(deviceLocation != null ? String.valueOf(deviceLocation.getLatitude()) : String.valueOf(0));
        attendance_log.setAttendanceLogLongitude(deviceLocation != null ? String.valueOf(deviceLocation.getLongitude()) : String.valueOf(0));
        attendance_log.setAttendanceLogSimCardSerial(telephonyManager.getSimSerialNumber());



        if(attendance_log.getAttendanceLogSimCardSerial() != null)
        {

            AttendanceLogRepository theRepository = new AttendanceLogRepository(new DatabaseHelper(this));
            int ret = theRepository.insert(attendance_log);

            //if it saved successfully
            if(ret != -1)
            {


                //record last time
                try
                {
                    SharedPreferences sharedPref = this.getSharedPreferences(this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(this.getString(R.string.last_attendance_logged_check_in_time), attendance_log.getAttendanceLogLogTime());
                    editor.commit();
                }
                catch(Exception e){
                    e.printStackTrace();
                }


                try{
                    AttendanceLogTransmitterService TransmitterService = new AttendanceLogTransmitterService();
                    Integer result = TransmitterService.SendLog(attendance_log);
                    switch(result){
                        case 200:case 409: {

                            //record last send time
                            try
                            {
                                theTime = dateFormat.format(NetworkTime.getNetworkDateTime(this));
                                SharedPreferences sharedPref = this.getSharedPreferences(this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString(this.getString(R.string.last_server_communication_time), theTime);
                                editor.commit();
                            }
                            catch(Exception e){
                                e.printStackTrace();
                            }

                            theRepository.delete(attendance_log.getAttendanceLogId());
                            break;
                        }
                        default: break;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }


            }


        }




    }





    public void getAllSms(final Activity activity) {


        CurrentThread = new Thread()
        {
            public void run()
            {

                try
                {


                    Uri message = Uri.parse("content://sms/");
                    ContentResolver cr = activity.getContentResolver();

                    try {

                        SharedPreferences ReaddsharedPref = activity.getSharedPreferences(activity.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                        String defaultValue = "Never";
                        String last_inbox_read_time = ReaddsharedPref.getString(activity.getString(R.string.last_inbox_read_time),defaultValue);

                        // Now create the filter and query the messages.
                        String filter = "";
                        String sorter = "date ASC";
                        if(last_inbox_read_time != "Never")
                        {
                            filter = "date>" + last_inbox_read_time;

                        }



                        Cursor c = cr.query(message, null, filter, null, sorter);
                        //claer the list
                        inbox_SmsList = new ArrayList<>();
                        int totalSMS = 0;
                        if (c != null) {
                            totalSMS = c.getCount();
                        }

                        if (c != null && c.moveToFirst()) {
                            for (int i = 0; i < totalSMS; i++) {

                                Sms objSms = new Sms();
                                objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                                objSms.setAddress(c.getString(c
                                        .getColumnIndexOrThrow("address")));
                                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
                                String the_time = c.getString(c.getColumnIndexOrThrow("date"));
                                long milli_seconds = Long.parseLong(the_time);
                                objSms.setTimeStr(dateFormat.format(new Date(milli_seconds)));
                                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                                    objSms.setFolderName("inbox");

                                    try {
                                        SaveSms(objSms);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    inbox_SmsList.add(objSms);


                                } else {
                                    objSms.setFolderName("sent");
                                }


                                c.moveToNext();
                            }
                        }
                        // else {
                        // throw new RuntimeException("You have no SMS");
                        // }
                        //c.close();

                        try {

                            //run the handler
                            ReadInboxComplete().run();
                        } catch (Exception exc)
                        {
                            exc.printStackTrace();
                        }



                    }
                    catch (Exception exc)
                    {
                        exc.printStackTrace();
                    }


                }
                catch (Exception e)
                {
                    Log.e("Thread Exception",e.getMessage());
                }


            }
        };
        CurrentThread.start();






    }


    private void SaveSms(Sms the_sms)
    {
        TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        Location deviceLocation = NetworkLocationService.previousBestLocation;
        if(deviceLocation == null){
            Log.i("SMS Receiver", "Device Location is null");
        }else{
            Log.i("SMS Receiver", "Device Location has a value");
        }

        SMSMessage smsMessage = new SMSMessage();
        smsMessage.setSMSMessageContent(the_sms.getMsg());
        smsMessage.setSMSMessageId(UUID.randomUUID().toString());
        smsMessage.setSMSMessageLogTime(the_sms.getTimeStr());

        smsMessage.setSMSMessageDeviceIMEI(telephonyManager.getDeviceId());
        smsMessage.setSMSMessageLatitude(deviceLocation != null ? String.valueOf(deviceLocation.getLatitude()) : String.valueOf(0));
        smsMessage.setSMSMessageLongitude(deviceLocation != null ? String.valueOf(deviceLocation.getLongitude()) : String.valueOf(0));
        smsMessage.setSMSMessageSimCardSerial(telephonyManager.getSimSerialNumber());
        smsMessage.setSMSMessageReferenceTime(the_sms.getTimeStr());


        if(smsMessage.getSMSMessageSimCardSerial() != null)
        {
            Log.i("SMS Receiver", smsMessage.toString());

            SMSRepository smsRepository = new SMSRepository(new DatabaseHelper(this));
            int ret = smsRepository.insert(smsMessage);

            //if it saved successfully
            if(ret != -1)
            {

                //record last time
                try
                {
                    SharedPreferences EditsharedPref = this.getBaseContext().getSharedPreferences(this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = EditsharedPref.edit();
                    editor.putString(this.getString(R.string.last_inbox_read_time), the_sms.getTime());
                    editor.commit();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

                //record last time
                try
                {
                    SharedPreferences sharedPref = this.getSharedPreferences(this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(this.getString(R.string.last_sms_recieved_time), the_sms.getTimeStr());
                    editor.commit();
                }
                catch(Exception e){
                    e.printStackTrace();
                }


            }


        }




    }




    private void UpdateInterface()
    {


        try
        {
            TextView imei = (TextView)this.findViewById(R.id.imei);
            TextView sim_serial = (TextView)this.findViewById(R.id.sim_serial);
            TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
            imei.setText(telephonyManager.getDeviceId());
            sim_serial.setText(telephonyManager.getSimSerialNumber());

            if(telephonyManager.getSimSerialNumber() == null)
            {
                Toast.makeText(Start.this,"Please Insert A Simcard",Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Info",exc.getMessage());
        }



        try
        {
            TextView last_recorded_on = (TextView)this.findViewById(R.id.last_recorded_on);
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_sms_recieved_time),defaultValue);
            last_recorded_on.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }



        try
        {
            TextView last_communicated_on = (TextView)this.findViewById(R.id.last_communicated_on);;
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_server_communication_time),defaultValue);
            last_communicated_on.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }


        try
        {
            TextView last_inbox_read = (TextView)this.findViewById(R.id.last_inbox_read);;
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_inbox_read_time),defaultValue);

            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
            if(value != "Never")
            {
                long milli_seconds = Long.parseLong(value);
                value = dateFormat.format(new Date(milli_seconds));
            }


            last_inbox_read.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }


        try
        {
            TextView last_attendance_log = (TextView)this.findViewById(R.id.last_attendance_log_check_in);
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_attendance_logged_check_in_time),defaultValue);

            LinearLayout lyt = (LinearLayout)this.findViewById(R.id.layout_add_attendance_log_check_in);

            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            if(value != "Never")
            {
                //get date component of today's date
                String today_date = dateFormat.format(NetworkTime.getNetworkDateTime(getBaseContext()));

                String previous_date = "";

                //get only date component from preious date
                try
                {
                    Date date_value = new Date(value);
                    previous_date = dateFormat.format(date_value);

                }
                catch (Exception exc)
                {
                    exc.printStackTrace();
                }

                if(previous_date.equals(today_date) )
                {
                    lyt.setVisibility(View.GONE);
                }
                else
                {
                    lyt.setVisibility(View.VISIBLE);
                }

            }
            else
            {

                //ask for the attendance_log

                lyt.setVisibility(View.VISIBLE);

            }


            last_attendance_log.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }


        try
        {
            TextView transaction_count = (TextView)this.findViewById(R.id.transaction_count);
            List<SMSMessage> smsMessageList;
            SMSRepository smsRepository = new SMSRepository(new DatabaseHelper(getBaseContext()));
            smsMessageList = smsRepository.getAllSMSMessages();

            List<AttendanceLog> attendanceLogList;
            AttendanceLogRepository logRepository = new AttendanceLogRepository(new DatabaseHelper(getBaseContext()));
            attendanceLogList = logRepository.getAllAttendanceLogs();

            if(smsMessageList.isEmpty() && attendanceLogList.isEmpty())
            {
                String none = "0";
                transaction_count.setText(none);
            }
            else
            {
                Integer the_size = smsMessageList.size() + attendanceLogList.size();
                transaction_count.setText(the_size.toString());
            }

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Sms cont",exc.getMessage());
        }



        try
        {
            TextView internet_availability = (TextView)this.findViewById(R.id.internet_availability);
            boolean isAvailable = internetIsAvailable();

            if(isAvailable)
            {
                String value  = "Yes";
                internet_availability.setText(value);
            }
            else
            {
                String value  = "No";
                internet_availability.setText(value);
            }

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }


    }



    private boolean internetIsAvailable(){
        try{
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch(Exception exc){
            exc.printStackTrace();
            return false;
        }
    }




    @Override
    protected void onResume()
    {

        try{
            super.onResume();

            UpdateInterface();

        }catch(Exception exc){
            exc.printStackTrace();
        }


    }

}
