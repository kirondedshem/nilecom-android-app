package androidcbs.chimstel.com.androidcbs;

public class AttendanceLog {

    public static String ATTENDANCE_LOG_TABLE = "attendance_log";

    public static String ATTENDANCE_LOG_ID = "id";
    public static String ATTENDANCE_LOG_LOG_TYPE = "log_type";
    public static String ATTENDANCE_LOG_DEVICE_IMEI = "device_imei";
    public static String ATTENDANCE_LOG_SIM_CARD_SERIAL = "sim_card_serial";
    public static String ATTENDANCE_LOG_LATITUDE = "latitude";
    public static String ATTENDANCE_LOG_LONGITUDE = "longitude";
    public static String ATTENDANCE_LOG_LOG_TIME = "log_time";


    private String AttendanceLogDeviceIMEI;
    private String AttendanceLogSimCardSerial;
    private String AttendanceLogId;
    private String AttendanceLogLogType;
    private String AttendanceLogLatitude;
    private String AttendanceLogLongitude;
    private String AttendanceLogLogTime;





    public String getAttendanceLogDeviceIMEI() {
        return AttendanceLogDeviceIMEI;
    }

    public void setAttendanceLogDeviceIMEI(String AttendanceLogDeviceIMEI) {
        this.AttendanceLogDeviceIMEI = AttendanceLogDeviceIMEI;
    }

    public String getAttendanceLogSimCardSerial() {
        return AttendanceLogSimCardSerial;
    }

    public void setAttendanceLogSimCardSerial(String AttendanceLogSimCardSerial) {
        this.AttendanceLogSimCardSerial = AttendanceLogSimCardSerial;
    }

    public String getAttendanceLogId() {
        return AttendanceLogId;
    }

    public void setAttendanceLogId(String AttendanceLogId) {
        this.AttendanceLogId = AttendanceLogId;
    }

    public String getAttendanceLogLogType() {
        return AttendanceLogLogType;
    }

    public void setAttendanceLogLogType(String AttendanceLogLogType) {
        this.AttendanceLogLogType = AttendanceLogLogType;
    }

    public String getAttendanceLogLatitude() {
        return AttendanceLogLatitude;
    }

    public void setAttendanceLogLatitude(String AttendanceLogLatitude) {
        this.AttendanceLogLatitude = AttendanceLogLatitude;
    }

    public String getAttendanceLogLongitude() {
        return AttendanceLogLongitude;
    }

    public void setAttendanceLogLongitude(String AttendanceLogLongitude) {
        this.AttendanceLogLongitude = AttendanceLogLongitude;
    }

    public String getAttendanceLogLogTime() {
        return AttendanceLogLogTime;
    }

    public void setAttendanceLogLogTime(String AttendanceLogLogTime) {
        this.AttendanceLogLogTime = AttendanceLogLogTime;
    }



    @Override
    public String toString() {
        return
                getAttendanceLogDeviceIMEI() + " : " + getAttendanceLogSimCardSerial() + " : " + getAttendanceLogId() + " : " +
                getAttendanceLogLogType() + " : " + getAttendanceLogLogTime() + " : " + getAttendanceLogLatitude() + " : " + getAttendanceLogLongitude();
    }
}
