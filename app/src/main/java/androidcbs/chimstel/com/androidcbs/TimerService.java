package androidcbs.chimstel.com.androidcbs;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimerService extends Service {

    private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    public TimerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId){
        final android.os.Handler handler = new android.os.Handler();

        handler.postDelayed(new Runnable() {
            @SuppressWarnings("deprecation")
            @Override
            public void run() {
                try {
                    Log.i("Period SMS Transmitter", "Sms transmitter onHandle Intent - runnable section");
                    // Date dateToday =TimeLogger.getInstance().getNetworkTime(TimerService.this);
                    Date dateToday = NetworkTime.getNetworkDateTime(TimerService.this);

                    double hour = dateToday.getHours()*1.0 +( dateToday.getMinutes()*1.0 / 60);
                    double morningHour = 8.0 ;
                    double eveningHour = 19.0+(27.0/60);
                    double nightHour = 20.0+(49.0/60);
                    //logWorkTime(TimerService.this);
                    //Toast.makeText(TimerService.this, "time this is the hour -> " + hour + ": " + (nightHour - (1.0 / 60)) + ":" + (nightHour + (1.0 / 60)), Toast.LENGTH_LONG).show();
                    if (hour >= morningHour - (1.0 / 60) && hour <= morningHour + (1.0 / 60)) {
                        logWorkTime(TimerService.this);
                    } else if (hour >= eveningHour - (1 / 60) && hour <= eveningHour + (1.0 / 60)) {
                        logWorkTime(TimerService.this);
                    } else if (hour >= nightHour - (1.0 / 60) && hour <= nightHour + (1.0 / 60)) {
                        logWorkTime(TimerService.this);
                    }

                    handler.postDelayed(this, 60 * 1000);
                }catch(Exception ex)
                {
                    Log.d("Time Exception" , ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }, 60 * 1000);
        return START_STICKY;
    }

    public void logWorkTime(Context context)
    {
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        Location deviceLocation = NetworkLocationService.previousBestLocation;
        if(deviceLocation == null){
            Log.i("SMS Receiver", "Device Location is null");
        }else{
            Log.i("SMS Receiver", "Device Location has a value");
        }
        LogObject logObject = new LogObject();

        String logMessageId = dateFormat.format(NetworkTime.getNetworkDateTime(context));
        String currentTime = dateFormat.format(NetworkTime.getNetworkDateTime(context));//    String.valueOf(NetworkTime.getNetworkDateTime(context).getTime());
        String simSerialNumber =telephonyManager.getSimSerialNumber();
        String deviceIMEI =telephonyManager.getDeviceId();
        String latitude = deviceLocation != null ? String.valueOf(deviceLocation.getLatitude()) : String.valueOf(0);
        String longitude =deviceLocation != null ? String.valueOf(deviceLocation.getLongitude()) : String.valueOf(0);
        String messageContent = "Device operating properly at " + currentTime;

        logObject.setLoggerMsgId(logMessageId);
        logObject.setSerial(simSerialNumber);
        logObject.setIMEI(deviceIMEI);
        logObject.setLatitude(latitude);
        logObject.setLongitude(longitude);
        logObject.setNetworkTime(currentTime);
        logObject.setMessageContent(messageContent);

        LoggerRepository loggerRepository = new LoggerRepository(new DatabaseHelper(context));
        loggerRepository.insert(logObject);
        //Toast.makeText(context, "time on me after insert", Toast.LENGTH_SHORT).show();
        //transfer data to backend
        try{
           // Toast.makeText(getApplicationContext(), "Before Transmission: " + logObject.toString(), Toast.LENGTH_SHORT).show();
            LogTransmitterService logTransmitterService = new LogTransmitterService();
            Integer result = logTransmitterService.sendLogObject(logObject);
            switch(result){
                case 200:case 409:
                   // Toast.makeText(getApplicationContext(), "Successfully logged data to the server", Toast.LENGTH_SHORT).show();
                    loggerRepository.delete(logObject.getLoggerMsgId());
                    break;
                default: break;
            }
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
